package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import efl.Eina_Rect
import io.gitlab.embedSoft.eflKt.core.Closable
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.pointed

public actual class EinaRectangle private constructor(ptr: CPointer<Eina_Rect>? = null): Closable {
    private val arena = Arena()
    public val struct: Eina_Rect = arena.alloc()

    public actual var xPos: Int
        get() = struct.x
        set(value) { struct.x = value }

    public actual var yPos: Int
        get() = struct.y
        set(value) { struct.y = value }

    public actual var width: Int
        get() = struct.w
        set(value) { struct.w = value }

    public actual var height: Int
        get() = struct.h
        set(value) { struct.h = value }

    init {
        if (ptr != null) {
            xPos = ptr.pointed.x
            yPos = ptr.pointed.y
            width = ptr.pointed.w
            height = ptr.pointed.h
        }
    }

    override fun close() {
        arena.clear()
    }

    public actual companion object {
        public actual fun create(): EinaRectangle = EinaRectangle()

        public fun fromPointer(ptr: CPointer<Eina_Rect>?): EinaRectangle = EinaRectangle(ptr)
    }
}
