package io.gitlab.embedSoft.eflKt.core.dataType

import efl.*
import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData
import kotlinx.cinterop.*

public actual class EinaHashTable private constructor(ptr: CPointer<Eina_Hash>?) : Closable, DisposableCallbackData {
    private val stableRef = StableRef.create(this)

    public val einaHashPtr: CPointer<Eina_Hash>? = ptr

    /** Contains the event handlers that are called by [forEach]. */
    public val forEachHandlers: EinaHashTableForEachEvent = EinaHashTableForEachEvent()

    /**
     * Adds an entry to the hash table.
     * @param key A unique key.
     * @param value The data to associate with the [key].
     * @return A value of *true* on success.
     */
    public fun add(key: COpaquePointer, value: COpaquePointer?): Boolean =
        eina_hash_add(hash = einaHashPtr, key = key, data = value) == true.uByteValue

    /**
     * Retrieves a specific entry from the hash table.
     * @param key The key of the entry to find.
     * @return The entry if it is found, or *null* if the entry isn't found.
     */
    public fun find(key: COpaquePointer): COpaquePointer? = eina_hash_find(einaHashPtr, key)

    /**
     * Retrieves a specific entry from the hash table.
     * @param key The key of the entry to find.
     * @return The entry if it is found, or *null* if the entry isn't found.
     */
    public operator fun get(key: COpaquePointer): COpaquePointer? = find(key)

    public actual fun forEach() {
        eina_hash_foreach(hash = einaHashPtr, func = staticCFunction(::hashTableForEachCallback),
            fdata = stableRef.asCPointer())
    }

    /**
     * Modifies the entry at the specified [key]. Will add the entry if the [key] doesn't exist.
     */
    public operator fun set(key: COpaquePointer, value: COpaquePointer) {
        eina_hash_set(hash = einaHashPtr, key = key, data = value)
    }

    public actual companion object {
        public const val DJB2_STR_ALGORITHM: String = "djb2"
        public const val SUPER_FAST_STR_ALGORITHM: String = "superfast"
        public const val STR_SMALL_STR_ALGORITHM: String = "stringsmall"

        public fun fromPointer(ptr: CPointer<Eina_Hash>?): EinaHashTable = EinaHashTable(ptr)

        public actual fun createWithStringKeys(algorithm: String): EinaHashTable =
            when (algorithm) {
                SUPER_FAST_STR_ALGORITHM -> EinaHashTable(eina_hash_string_superfast_new(null))
                STR_SMALL_STR_ALGORITHM -> EinaHashTable(eina_hash_string_small_new(null))
                else -> EinaHashTable(eina_hash_string_djb2_new(null))
            }

        public actual fun createWithIntKeys(): EinaHashTable = EinaHashTable(eina_hash_int32_new(null))

        public actual fun createWithLongKeys(): EinaHashTable = EinaHashTable(eina_hash_int64_new(null))
    }

    override fun close() {
        eina_hash_free(einaHashPtr)
        stableRef.dispose()
    }

    public override fun disposeStableReference() {
        stableRef.dispose()
    }
}

private fun hashTableForEachCallback(
    @Suppress("UNUSED_PARAMETER") hashTable: CPointer<Eina_Hash>?,
    key: COpaquePointer?,
    value: COpaquePointer?,
    userData: COpaquePointer?
): UByte {
    val tmp = userData?.asStableRef<EinaHashTable>()?.get()
    tmp?.forEachHandlers?.invoke(key, value)
    return true.uByteValue
}

public fun CPointer<Eina_Hash>?.toEinaHashTable(): EinaHashTable = EinaHashTable.fromPointer(this)
