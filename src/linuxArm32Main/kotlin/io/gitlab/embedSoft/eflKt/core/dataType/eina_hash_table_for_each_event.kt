package io.gitlab.embedSoft.eflKt.core.dataType

import kotlinx.cinterop.COpaquePointer

public typealias EinaHashTableForEachHandler = (key: COpaquePointer?, value: COpaquePointer?) -> Unit

@Suppress("SuspiciousCollectionReassignment")
public class EinaHashTableForEachEvent {
    private var handlers = emptyList<EinaHashTableForEachHandler>()

    public fun subscribe(handler: EinaHashTableForEachHandler) {
        handlers += handler
    }

    public fun unsubscribe(handler: EinaHashTableForEachHandler) {
        handlers -= handler
    }

    public operator fun plusAssign(handler: EinaHashTableForEachHandler): Unit = subscribe(handler)

    public operator fun minusAssign(handler: EinaHashTableForEachHandler): Unit = unsubscribe(handler)

    public operator fun invoke(key: COpaquePointer?, value: COpaquePointer?) {
        var exception: Throwable? = null
        for (handler in handlers) {
            try {
                handler(key, value)
            } catch (ex: Throwable) {
                exception = ex
            }
        }
        if (exception != null) throw exception
    }
}
