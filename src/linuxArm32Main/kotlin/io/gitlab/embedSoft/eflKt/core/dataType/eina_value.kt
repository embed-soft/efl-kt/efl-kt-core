package io.gitlab.embedSoft.eflKt.core.dataType

import efl.*
import io.gitlab.embedSoft.eflKt.core.Closable
import kotlinx.cinterop.*
import platform.linux.free

public actual class EinaValue private constructor(ptr: CPointer<Eina_Value>?): Closable {
    public val einaValuePtr: CPointer<Eina_Value>? = ptr

    override fun close() {
        eina_value_free(einaValuePtr)
    }

    public actual fun getString(): String {
        val ptr = eina_value_to_string(einaValuePtr)
        val result = ptr?.toKString() ?: ""
        free(ptr)
        return result
    }

    public actual fun setString(newValue: String) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getChar(): Char = memScoped {
        val tmp = alloc<ByteVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value.toInt().toChar()
    }

    public actual fun setChar(newValue: Char) {
        eina_value_set(einaValuePtr, newValue.code.toByte())
    }

    public actual fun getBoolean(): Boolean = memScoped {
        // TODO: Fix this function.
        val tmp = alloc<UByteVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value.booleanValue
    }

    public actual fun setBoolean(newValue: Boolean) {
        // TODO: Fix this function.
        eina_value_set(einaValuePtr, newValue.uByteValue)
    }

    public actual fun getDouble(): Double = memScoped {
        val tmp = alloc<DoubleVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setDouble(newValue: Double) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getFloat(): Float = memScoped {
        val tmp = alloc<FloatVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setFloat(newValue: Float) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getShort(): Short = memScoped {
        val tmp = alloc<ShortVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setShort(newValue: Short) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getUShort(): UShort = memScoped {
        val tmp = alloc<UShortVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setUShort(newValue: UShort) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getInt(): Int = memScoped {
        val tmp = alloc<IntVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setInt(newValue: Int) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getUInt(): UInt = memScoped {
        val tmp = alloc<UIntVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setUInt(newValue: UInt) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual companion object {
        public fun fromPointer(ptr: CPointer<Eina_Value>?): EinaValue = EinaValue(ptr)

        public actual fun createStringValue(initialValue: String): EinaValue =
            EinaValue(eina_value_string_new(initialValue))

        public actual fun createCharValue(initialValue: Char): EinaValue =
            EinaValue(eina_value_char_new(initialValue.code.toByte()))

        public actual fun createBooleanValue(initialValue: Boolean): EinaValue =
            EinaValue(eina_value_bool_new(initialValue.uByteValue))

        public actual fun createDoubleValue(initialValue: Double): EinaValue =
            EinaValue(eina_value_double_new(initialValue))

        public actual fun createFloatValue(initialValue: Float): EinaValue =
            EinaValue(eina_value_float_new(initialValue))

        public actual fun createShortValue(initialValue: Short): EinaValue =
            EinaValue(eina_value_short_new(initialValue))

        public actual fun createUShortValue(initialValue: UShort): EinaValue =
            EinaValue(eina_value_ushort_new(initialValue))

        public actual fun createIntValue(initialValue: Int): EinaValue =
            EinaValue(eina_value_int_new(initialValue))

        public actual fun createUIntValue(initialValue: UInt): EinaValue =
            EinaValue(eina_value_uint_new(initialValue))

        public actual fun createLongValue(initialValue: Long): EinaValue =
            EinaValue(eina_value_int64_new(initialValue))

        public actual fun createULongValue(initialValue: ULong): EinaValue =
            EinaValue(eina_value_uint64_new(initialValue))
    }

    public actual fun getULong(): ULong = memScoped {
        val tmp = alloc<ULongVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setULong(newValue: ULong) {
        eina_value_set(einaValuePtr, newValue)
    }

    public actual fun getLong(): Long = memScoped {
        val tmp = alloc<LongVar>()
        eina_value_get(einaValuePtr, tmp.ptr)
        return tmp.value
    }

    public actual fun setLong(newValue: Long) {
        eina_value_set(einaValuePtr, newValue)
    }
}

public fun CPointer<Eina_Value>?.toEinaValue(): EinaValue = EinaValue.fromPointer(this)
