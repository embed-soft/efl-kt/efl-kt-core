package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import efl.Eina_Position2D
import io.gitlab.embedSoft.eflKt.core.Closable
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.pointed

public actual class EinaPosition2D private constructor(ptr: CPointer<Eina_Position2D>? = null): Closable {
    private val arena = Arena()
    public val struct: Eina_Position2D = arena.alloc()

    public actual var xPos: Int
        get() = struct.x
        set(value) { struct.x = value }

    public actual var yPos: Int
        get() = struct.y
        set(value) { struct.y = value }

    init {
        if (ptr != null) {
            xPos = ptr.pointed.x
            yPos = ptr.pointed.y
        }
    }

    public actual companion object {
        public actual fun create(): EinaPosition2D = EinaPosition2D()

        public fun fromPointer(ptr: CPointer<Eina_Position2D>?): EinaPosition2D = EinaPosition2D(ptr)
    }

    override fun close() {
        arena.clear()
    }
}
