package io.gitlab.embedSoft.eflKt.core.dataType

import efl.*
import io.gitlab.embedSoft.eflKt.core.Closable
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer

public actual class EinaList private constructor(ptr: CPointer<Eina_List>? = null): Closable {
    private var _einaListPtr = ptr

    public val einaListPtr: CPointer<Eina_List>?
        get() = _einaListPtr

    public actual val size: UInt
        get() = eina_list_count(_einaListPtr)

    /**
     * Appends [data] to the list.
     * @param data The data to append.
     */
    public infix fun append(data: COpaquePointer) {
        _einaListPtr = eina_list_append(_einaListPtr, data)
    }

    /**
     * Prepends [data] to the list.
     * @param data The data to prepend.
     */
    public infix fun prepend(data: COpaquePointer) {
        _einaListPtr = eina_list_prepend(_einaListPtr, data)
    }

    /**
     * Removes the first instance of [data] from the list.
     * @param data The data to remove.
     */
    public infix fun remove(data: COpaquePointer) {
        _einaListPtr = eina_list_remove(_einaListPtr, data)
    }

    /**
     * Finds a member of the list and returns it.
     * @param data The data to find.
     * @return The data if found or *null*.
     */
    public infix fun findData(data: COpaquePointer): COpaquePointer? = eina_list_data_find(_einaListPtr, data)

    /**
     * Gets the data in the list by [index].
     * @param index The list index to use.
     * @return The data in the list or *null*.
     */
    public operator fun get(index: UInt): COpaquePointer? = eina_list_nth(_einaListPtr, index)

    public actual companion object {
        public fun fromPointer(ptr: CPointer<Eina_List>?): EinaList = EinaList(ptr)

        public actual fun create(): EinaList = EinaList()
    }

    override fun close() {
        if (_einaListPtr != null) eina_list_free(_einaListPtr)
        _einaListPtr = null
    }

    public actual fun reverse() {
        _einaListPtr = eina_list_reverse(_einaListPtr)
    }

    /**
     * Sorts the list according to the ordering [func] will return.
     * @param limit The maximum number of elements to sort.
     * @param func The function to use for comparing the elements.
     */
    public fun sort(limit: UInt, func: Eina_Compare_Cb) {
        _einaListPtr = eina_list_sort(list = _einaListPtr, limit = limit, func = func)
    }
}

public fun CPointer<Eina_List>?.toEinaList(): EinaList = EinaList.fromPointer(this)
