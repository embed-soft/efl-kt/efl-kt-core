package io.gitlab.embedSoft.eflKt.core.dataType

import efl.*
import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData
import io.gitlab.embedSoft.eflKt.core.Event
import kotlinx.cinterop.*

public actual class EinaIterator private constructor(ptr: CPointer<Eina_Iterator>?): Closable, DisposableCallbackData {
    public val einaIteratorPtr: CPointer<Eina_Iterator>? = ptr

    /** The container that the iterator uses. */
    public val container: COpaquePointer?
        get() = eina_iterator_container_get(einaIteratorPtr)

    private val stableRef = StableRef.create(this)

    /** Contains the event handlers that are called by [forEach]. */
    public val forEachHandlers: Event<COpaquePointer?> = Event()

    /**
     * Gets the value of the current element, and goes to the next one.
     * @return A Pair containing the following:
     * 1. success - Will be *true* if the operation was successful.
     * 2. data - The data of the element.
     */
    public fun next(): Pair<Boolean, COpaquePointer?> = memScoped {
        val data = alloc<COpaquePointerVar>()
        val success = eina_iterator_next(einaIteratorPtr, data.ptr).booleanValue
        success to data.value
    }

    override fun close() {
        eina_iterator_free(einaIteratorPtr)
        stableRef.dispose()
    }

    override fun disposeStableReference() {
        stableRef.dispose()
    }

    public companion object {
        public fun fromPointer(ptr: CPointer<Eina_Iterator>?): EinaIterator = EinaIterator(ptr)
    }

    public actual fun forEach() {
        eina_iterator_foreach(iterator = einaIteratorPtr, callback = staticCFunction(::iteratorForEachCallback),
            fdata = stableRef.asCPointer())
    }
}

private fun iteratorForEachCallback(
    @Suppress("UNUSED_PARAMETER") container: COpaquePointer?,
    containerData: COpaquePointer?,
    userData: COpaquePointer?
): UByte {
    val tmp = userData?.asStableRef<EinaArray>()?.get()
    tmp?.forEachHandlers?.invoke(containerData)
    return true.uByteValue
}

public fun CPointer<Eina_Iterator>?.toEinaIterator(): EinaIterator = EinaIterator.fromPointer(this)
