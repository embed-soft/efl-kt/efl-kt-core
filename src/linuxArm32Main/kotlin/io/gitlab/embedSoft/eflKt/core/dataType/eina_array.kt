package io.gitlab.embedSoft.eflKt.core.dataType

import efl.*
import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData
import io.gitlab.embedSoft.eflKt.core.Event
import io.gitlab.embedSoft.eflKt.core.strValue
import kotlinx.cinterop.*

public actual class EinaArray private constructor(ptr: CPointer<Eina_Array>?): Closable, DisposableCallbackData {
    private val stableRef = StableRef.create(this)

    public val einaArrayPtr: CPointer<Eina_Array>? = ptr

    public actual val size: UInt
        get() = eina_array_count(einaArrayPtr)

    /** Contains the event handlers that are called by [forEach]. */
    public val forEachHandlers: Event<COpaquePointer?> = Event()

    public actual fun forEach() {
        eina_array_foreach(array = einaArrayPtr, cb = staticCFunction(::arrayForEachCallback),
            fdata = stableRef.asCPointer())
    }

    /**
     * Gets an element from the array by [index].
     * @param index The index to use in the array.
     * @return The element.
     */
    public operator fun get(index: UInt): COpaquePointer? = eina_array_data_get(einaArrayPtr, index)

    public actual infix fun getString(index: UInt): String = eina_array_data_get(einaArrayPtr, index).strValue

    /**
     * Changes an element in the array by [index].
     * @param index The index to use in the array.
     * @param data The new data to use as the element.
     */
    public operator fun set(index: UInt, data: COpaquePointer?) {
        eina_array_data_set(array = einaArrayPtr, data = data, idx = index)
    }

    /**
     * Adds a new element to the end of the array.
     * @param data The data to add.
     */
    public operator fun plusAssign(data: COpaquePointer?) {
        push(data)
    }

    /**
     * Adds a new element to the end of the array.
     * @param data The data to add.
     * @return A value of *true* on success, or *false* on failure.
     */
    public fun push(data: COpaquePointer?): Boolean = eina_array_push(einaArrayPtr, data).booleanValue

    public actual companion object {
        public fun fromPointer(ptr: CPointer<Eina_Array>?): EinaArray = EinaArray(ptr)

        public actual fun create(size: UInt): EinaArray = EinaArray(eina_array_new(size))
    }

    public override fun disposeStableReference() {
        stableRef.dispose()
    }

    override fun close() {
        eina_array_free(einaArrayPtr)
        stableRef.dispose()
    }
}

private fun arrayForEachCallback(
    @Suppress("UNUSED_PARAMETER") array: COpaquePointer?,
    arrayData: COpaquePointer?,
    userData: COpaquePointer?
): UByte {
    val tmp = userData?.asStableRef<EinaArray>()?.get()
    tmp?.forEachHandlers?.invoke(arrayData)
    return true.uByteValue
}

public fun CPointer<Eina_Array>?.toEinaArray(): EinaArray = EinaArray.fromPointer(this)
