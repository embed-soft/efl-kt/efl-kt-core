package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import efl.Eina_Size2D
import io.gitlab.embedSoft.eflKt.core.Closable
import kotlinx.cinterop.Arena
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.alloc
import kotlinx.cinterop.pointed

public actual class EinaSize2D private constructor(ptr: CPointer<Eina_Size2D>? = null): Closable {
    private val arena = Arena()
    public val struct: Eina_Size2D = arena.alloc()

    public actual var width: Int
        get() = struct.w
        set(value) { struct.w = value }

    public actual var height: Int
        get() = struct.h
        set(value) { struct.h = value }

    init {
        if (ptr != null) {
            width = ptr.pointed.w
            height = ptr.pointed.h
        }
    }

    override fun close() {
        arena.clear()
    }

    public actual companion object {
        public actual fun create(): EinaSize2D = EinaSize2D()

        public fun fromPointer(ptr: CPointer<Eina_Size2D>?): EinaSize2D = EinaSize2D(ptr)
    }
}
