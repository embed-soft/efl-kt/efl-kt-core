package io.gitlab.embedSoft.eflKt.core

import efl.ecore_init
import efl.ecore_init_ex
import efl.ecore_shutdown
import efl.ecore_shutdown_ex
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toCStringArray

public actual fun initEcoreEx(args: Array<String>): UInt = memScoped {
    return ecore_init_ex(args.size, args.toCStringArray(this))
}

public actual fun initEcore(): Int = ecore_init()

public actual fun shutdownEcore(): Int = ecore_shutdown()

public actual fun shutdownEcoreEx(): UInt = ecore_shutdown_ex()
