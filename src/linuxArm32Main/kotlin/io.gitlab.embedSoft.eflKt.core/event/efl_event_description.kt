package io.gitlab.embedSoft.eflKt.core.event

import efl.Efl_Event_Description
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString

public fun CPointer<Efl_Event_Description>?.toEflEventDescription(): EflEventDescription {
    if (this == null) throw IllegalStateException("EFL event description cannot be null")
    val tmp = this.pointed
    return EflEventDescription(
        name = EflEventName(tmp.name?.toKString() ?: ""),
        unfreezable = tmp.unfreezable.booleanValue,
        restart = tmp.restart.booleanValue
    )
}
