package io.gitlab.embedSoft.eflKt.core.event

import efl.Efl_Event
import io.gitlab.embedSoft.eflKt.core.EflObjectBase
import io.gitlab.embedSoft.eflKt.core.toEflObject
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed

/** Provides event meta data. */
public data class EflEvent(
    val desc: EflEventDescription,
    val obj: EflObjectBase,
    val info: COpaquePointer?
)

public fun CPointer<Efl_Event>?.toEflEvent(): EflEvent {
    if (this == null) throw IllegalStateException("EFL event cannot be null")
    val tmp = this.pointed
    return EflEvent(
        /** Event description. */
        desc = tmp.desc.toEflEventDescription(),
        /** The object that this event covers. */
        obj = tmp.`object`.toEflObject(),
        /** Event data. */
        info = tmp.info
    )
}

public typealias EflEventHandler = (EflEvent) -> Unit
