package io.gitlab.embedSoft.eflKt.core.event

import efl.Efl_Event
import efl.Efl_Event_Description
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData
import kotlinx.cinterop.*

public actual class EflEventHandlerRegistry : DisposableCallbackData {
    internal val handlerFuncPtr = staticCFunction(::handleEvent)
    private val stableRef: StableRef<EflEventHandlerRegistry> = StableRef.create(this)
    internal val stableRefPtr = stableRef.asCPointer()
    private val handlers: MutableMap<EflEventName, EflEventHandler> = mutableMapOf()

    public fun register(eventName: EflEventName, newHandler: EflEventHandler) {
        handlers[eventName] = newHandler
    }

    public fun register(desc: CPointer<Efl_Event_Description>?, newHandler: EflEventHandler) {
        handlers[desc.toEflEventDescription().name] = newHandler
    }

    public actual fun unregister(eventName: EflEventName): Boolean = handlers.remove(eventName) != null

    public fun unregister(desc: CPointer<Efl_Event_Description>?) {
        handlers.remove(desc.toEflEventDescription().name)
    }

    public fun findEventHandler(eventName: EflEventName): EflEventHandler? = handlers[eventName]

    override fun disposeStableReference() {
        stableRef.dispose()
    }
}

private fun handleEvent(data: COpaquePointer?, event: CPointer<Efl_Event>?) {
    val tmpObj = data?.asStableRef<EflEventHandlerRegistry>()?.get()
    val tmpEvent = event.toEflEvent()
    val handler = tmpObj?.findEventHandler(tmpEvent.desc.name)
    if (handler != null) handler(tmpEvent)
}
