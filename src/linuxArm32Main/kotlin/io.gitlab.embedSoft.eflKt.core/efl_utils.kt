package io.gitlab.embedSoft.eflKt.core

import efl.*
import io.gitlab.embedSoft.eflKt.core.dataType.toEinaArray
import io.gitlab.embedSoft.eflKt.core.event.EflEvent
import io.gitlab.embedSoft.eflKt.core.event.EflEventHandler
import io.gitlab.embedSoft.eflKt.core.event.toEflEvent
import kotlinx.cinterop.pointed
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction

private var mainLoopHandler: EflEventHandler = {}

public fun changeMainLoopHandler(handler: EflEventHandler) {
    mainLoopHandler = handler
}

public actual fun exitApplication(exitCode: Int) {
    efl_exit(exitCode)
}

public actual fun addMainLoopHandler(): Boolean =
    getMainLoop().addEventHandler(EFL_LOOP_EVENT_ARGUMENTS,
        staticCFunction { _, event -> mainLoopHandler(event.toEflEvent()) })

/**
 * Starts the CLI application.
 * @param args The program arguments to use.
 * @param handler The event handler for the main event loop.
 */
public fun startCliApplication(args: Array<String>, handler: EflEventHandler) {
    initEcore()
    changeMainLoopHandler(handler)
    addMainLoopHandler()
    initEcoreEx(args)
    beginMainLoop()
}

/**
 * Gets the program arguments.
 * @return Program arguments in an array.
 */
public fun EflEvent.fetchProgramArguments(): Array<String> {
    val tmpResult = mutableListOf<String>()
    val eventLoopArgs = info?.reinterpret<Efl_Loop_Arguments>()
    val tmpArray = eventLoopArgs?.pointed?.argv.toEinaArray()
    tmpArray.forEachHandlers.subscribe { tmpResult += it.strValue }
    tmpArray.forEach()
    return tmpResult.toTypedArray()
}
