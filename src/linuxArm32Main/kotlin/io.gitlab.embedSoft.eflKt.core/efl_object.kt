package io.gitlab.embedSoft.eflKt.core

import efl.*
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import kotlinx.cinterop.CPointer

public actual class EflObject private constructor(ptr: CPointer<Eo>?) : EflObjectBase {
    public override val eoPtr: CPointer<Eo>? = ptr

    public companion object {
        /**
         * Creates a new object that is owned by the [parent].
         * @param classType The class type to use for the new object.
         * @param parent The parent object that owns the new object, or *null*.
         * @param init The lambda that will initialize the new object.
         * @return The newly created object.
         */
        public fun create(
            classType: CPointer<Efl_Class>?,
            parent: EflObjectBase,
            init: EflObjectBase.() -> Unit
        ): EflObject = EflObject(createCommon(classType = classType, parent = parent, init = init))

        /**
         * Creates a new object that has an extra reference to it.
         * @param classType The class type to use for the new object.
         * @param parent The parent object that owns the new object, or *null*.
         * @param init The lambda that will initialize the new object.
         * @return The newly created object.
         */
        public fun createWithReference(
            classType: CPointer<Efl_Class>?,
            parent: EflObjectBase?,
            init: EflObjectBase.() -> Unit
        ): EflObject = EflObject(createCommon(classType = classType, parent = parent, isRef = true, init = init))

        private fun createCommon(
            classType: CPointer<Efl_Class>?,
            parent: EflObjectBase?,
            isRef: Boolean = false,
            init: EflObjectBase.() -> Unit
        ): CPointer<Eo> {
            val obj = _efl_add_internal_start(
                file = "",
                line = -1,
                klass_id = classType,
                parent = parent?.eoPtr,
                ref = isRef.uByteValue,
                is_fallback = true.uByteValue
            )
            init(fromPointer(obj))
            return _efl_add_end(obj = _efl_added_get(), is_ref = isRef.uByteValue, is_fallback = true.uByteValue)
                ?: throw IllegalStateException("The created object cannot be null")
        }

        public fun fromPointer(ptr: CPointer<Eo>?): EflObject = EflObject(ptr)
    }
}

public fun CPointer<Eo>?.toEflObject(): EflObject = EflObject.fromPointer(this)
