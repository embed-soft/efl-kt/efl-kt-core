package io.gitlab.embedSoft.eflKt.core

import efl.eina_init

public actual fun initEina(): Int = eina_init()
