package io.gitlab.embedSoft.eflKt.core

import efl.Efl_Version
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import kotlinx.cinterop.toKString

public fun CPointer<Efl_Version>?.toEflVersion(): EflVersion {
    if (this == null) throw IllegalStateException("EFL version cannot be null")
    val tmp = this.pointed
    return EflVersion(
        major = tmp.major,
        minor = tmp.minor,
        micro = tmp.micro,
        revision = tmp.revision,
        flavor = tmp.flavor?.toKString() ?: "",
        buildId = tmp.build_id?.toKString() ?: ""
    )
}
