package io.gitlab.embedSoft.eflKt.core

import efl.efl_loop_begin
import efl.efl_main_loop_get
import io.gitlab.embedSoft.eflKt.core.dataType.EinaValue

public actual fun beginLoop(obj: EflObject): EinaValue = EinaValue.fromPointer(efl_loop_begin(obj.eoPtr))

public actual fun getMainLoop(): EflObject = EflObject.fromPointer(efl_main_loop_get())

public actual fun beginMainLoop() {
    beginLoop(getMainLoop())
}
