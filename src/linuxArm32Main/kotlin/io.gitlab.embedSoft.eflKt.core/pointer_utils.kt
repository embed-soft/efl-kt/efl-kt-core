package io.gitlab.embedSoft.eflKt.core

import kotlinx.cinterop.*

public val COpaquePointer?.strValue: String
    get() = this?.reinterpret<ByteVar>()?.toKString() ?: ""

public val COpaquePointer?.uByteValue: UByte
    get() = this?.reinterpret<UByteVar>()?.pointed?.value ?: 0u.toUByte()

public val COpaquePointer?.intValue: Int
    get() = this?.reinterpret<IntVar>()?.pointed?.value ?: 0

public val COpaquePointer?.uIntValue: UInt
    get() = this?.reinterpret<UIntVar>()?.pointed?.value ?: 0u

public val COpaquePointer?.longValue: Long
    get() = this?.reinterpret<LongVar>()?.pointed?.value ?: 0L

public val COpaquePointer?.uLongValue: ULong
    get() = this?.reinterpret<ULongVar>()?.pointed?.value ?: 0uL

public val COpaquePointer?.floatValue: Float
    get() = this?.reinterpret<FloatVar>()?.pointed?.value ?: 0F

public val COpaquePointer?.doubleValue: Double
    get() = this?.reinterpret<DoubleVar>()?.pointed?.value ?: 0.0

public val COpaquePointer?.shortValue: Short
    get() = this?.reinterpret<ShortVar>()?.pointed?.value ?: 0.toShort()

public val COpaquePointer?.uShortValue: UShort
    get() = this?.reinterpret<UShortVar>()?.pointed?.value ?: 0u.toUShort()
