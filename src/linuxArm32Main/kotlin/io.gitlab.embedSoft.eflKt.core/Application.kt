package io.gitlab.embedSoft.eflKt.core

import efl.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

public actual class Application private constructor(ptr: CPointer<Efl_App>?): EflObjectBase {
    public val eflAppPtr: CPointer<Efl_App>? = ptr

    override val eoPtr: CPointer<Eo>?
        get() = eflAppPtr?.reinterpret()

    public actual val eflBuildVersion: EflVersion
        get() = efl_app_build_efl_version_get(eoPtr).toEflVersion()

    public actual val eflVersion: EflVersion
        get() = efl_app_efl_version_get(eoPtr).toEflVersion()


    public actual companion object {
        public fun fromPointer(ptr: CPointer<Efl_App>?): Application = Application(ptr)
        public actual fun getMain(): Application = Application(efl_app_main_get())
    }
}
