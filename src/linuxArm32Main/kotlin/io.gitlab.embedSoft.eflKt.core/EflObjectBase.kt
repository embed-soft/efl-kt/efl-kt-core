package io.gitlab.embedSoft.eflKt.core

import efl.*
import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.booleanValue
import io.gitlab.embedSoft.eflKt.core.dataType.toEinaIterator
import io.gitlab.embedSoft.eflKt.core.dataType.uByteValue
import io.gitlab.embedSoft.eflKt.core.event.EflEventHandlerRegistry
import io.gitlab.embedSoft.eflKt.core.event.toEflEvent
import kotlinx.cinterop.*

public actual interface EflObjectBase {
    public val eoPtr: CPointer<Eo>?

    public actual val invalidated: Boolean
        get() = efl_invalidated_get(eoPtr).booleanValue

    public actual val invalidating: Boolean
        get() = efl_invalidating_get(eoPtr).booleanValue

    public actual val freezeEventCount: Int
        get() = efl_event_freeze_count_get(eoPtr)

    public actual var name: EflObjectName
        get() = EflObjectName(efl_name_get(eoPtr)?.toKString() ?: "")
        set(value) = efl_name_set(eoPtr, value.name)

    public actual var parent: EflObjectBase?
        get() = efl_parent_get(eoPtr)?.toEflObject()
        set(value) = efl_parent_set(eoPtr, value?.eoPtr)

    public actual val refCount: Int
        get() = efl_ref_count(eoPtr)

    /**
     * Add an event callback forwarder that will make this object emit an event whenever another object ([source])
     * emits it. The event is said to be forwarded from source to this object. Note that the event is unaffected on
     * [source] and behaves like any other event being propagated on any object, and will trigger all the event
     * handlers registered on [source] like nothing special happened.
     *
     * This allows an object that hides internally another object to easily be able to propagate an event without the
     * need to add a custom event handler.
     * @param desc The event description.
     * @param source The object which fires the initial event.
     * @param priority The priority at which to insert the event forwarder handler in the existing list of handler on
     * the source of event object. The lower the number, the higher the priority.
     */
    public fun addEventHandlerForwarder(
        desc: CPointer<Efl_Event_Description>?,
        source: EflObjectBase,
        priority: Short = EFL_CALLBACK_PRIORITY_DEFAULT.toShort()
    ) {
        efl_event_callback_forwarder_priority_add(obj = eoPtr, desc = desc, priority = priority, source = source.eoPtr)
    }

    /**
     * Adds an event handler to this object.
     * @param desc The event description.
     * @param handler The event handler to add.
     * @param data User data to pass to the [handler].
     * @param priority The priority of the event handler. The lower the number, the higher the priority.
     * @return A value of *true* if the [handler] was successfully added.
     */
    public fun addEventHandler(
        desc: CPointer<Efl_Event_Description>?,
        handler: Efl_Event_Cb,
        data: COpaquePointer? = null,
        priority: Short = EFL_CALLBACK_PRIORITY_DEFAULT.toShort()
    ): Boolean = efl_event_callback_priority_add(
        obj = eoPtr,
        desc = desc,
        priority = priority,
        cb = handler,
        data = data
    ) == true.uByteValue

    /**
     * Removes an event handler from this object that was supplied by a [registry].
     * @param desc The event description.
     * @param handler The event handler to remove.
     * @param registry User data to pass to the [handler].
     * @return A value of *true* if the [handler] was successfully removed.
     */
    public fun removeEventHandler(
        desc: CPointer<Efl_Event_Description>?,
        handler: Efl_Event_Cb,
        registry: EflEventHandlerRegistry
    ): Boolean = efl_event_callback_del(
        desc = desc, obj = eoPtr, func = registry.handlerFuncPtr, user_data = registry.stableRefPtr
    ) == true.uByteValue

    /**
     * Adds an event handler to this object from the [registry].
     * @param desc The event description.
     * @param registry The registry that contains the event handler for the object.
     * @param priority The priority of the event handler. The lower the number, the higher the priority.
     * @return A value of *true* if the event handler was successfully added from the [registry].
     */
    public fun addEventHandler(
        desc: CPointer<Efl_Event_Description>?,
        registry: EflEventHandlerRegistry,
        priority: Short = EFL_CALLBACK_PRIORITY_DEFAULT.toShort()
    ): Boolean = efl_event_callback_priority_add(
        obj = eoPtr,
        desc = desc,
        priority = priority,
        cb = registry.handlerFuncPtr,
        data = registry.stableRefPtr
    ) == true.uByteValue

    /**
     * Removes an event handler from this object.
     * @param desc The event description.
     * @param handler The event handler to remove.
     * @param data User data to pass to the [handler].
     * @return A value of *true* if the [handler] was successfully removed.
     */
    public fun removeEventHandler(
        desc: CPointer<Efl_Event_Description>?,
        handler: Efl_Event_Cb,
        data: COpaquePointer? = null
    ): Boolean = efl_event_callback_del(
        desc = desc, obj = eoPtr, func = handler, user_data = data
    ) == true.uByteValue

    /**
     * Adds a weak reference to this object.
     * @param weakRef The designated weak reference.
     */
    public fun addWeakReference(weakRef: CPointer<CPointerVar<Eo>>) {
        efl_wref_add(eoPtr, weakRef)
    }

    public actual fun unReference() {
        efl_unref(eoPtr)
    }

    public actual var comment: String
        get() = efl_comment_get(eoPtr)?.toKString() ?: ""
        set(value) = efl_comment_set(eoPtr, value)

    public actual fun freezeAllEvents() {
        efl_event_freeze(eoPtr)
    }

    public actual fun thawAllEvents() {
        efl_event_thaw(eoPtr)
    }

    /**
     * Searches upwards in the object tree for a provider which knows the given class/interface.
     * @param classType The class/interface to look for.
     * @return An efl object or *null* if the provider isn't found.
     */
    public fun findProviderByClass(classType: CPointer<Efl_Class>?): EflObjectBase? =
        efl_provider_find(eoPtr, classType)?.toEflObject()

    /**
     * Will register a manager of a specific class to be answered by [findProviderByClass].
     * @param classType The type of class/interface to register.
     * @param provider The object representing the provider.
     * @return A value of *true* if [provider] has been registered.
     * @since EFL v1.22
     */
    public fun registerProvider(classType: CPointer<Efl_Class>?, provider: EflObjectBase): Boolean =
        efl_provider_register(obj = eoPtr, klass = classType, provider = provider.eoPtr) == true.uByteValue

    /**
     * Will unregister a manager of a specific class that was previously registered and answered by
     * [findProviderByClass].
     * @param classType The type of class/interface to unregister.
     * @param provider The object representing the provider.
     * @return A value of *true* if [provider] has been unregistered.
     */
    public fun unregisterProvider(classType: CPointer<Efl_Class>?, provider: EflObjectBase): Boolean =
        efl_provider_unregister(obj = eoPtr, klass = classType, provider = provider.eoPtr) == true.uByteValue

    public actual companion object {
        public actual fun thawAllGlobalEvents() {
            efl_event_global_thaw()
        }

        public actual fun freezeAllGlobalEvents() {
            efl_event_global_freeze()
        }
    }

    /**
     * Searches upwards in the object tree for a provider which knows the given class/interface. The object from the
     * provider will then be returned. The base implementation calls the [findProvider] function on the object parent,
     * and returns its result. If no parent is present *null* is returned. Each implementation has to support this
     * function by overriding it, and returning itself if the interface matches the parameter. If this isn't done the
     * class cannot be found up in the object tree.
     * @param classType The class identifier to search for.
     * @return The provider or *null* if the provider isn't found.
     * @since EFL v1.22
     */
    public fun findProvider(classType: CPointer<Efl_Class>?): EflObjectBase? =
        efl_provider_find(eoPtr, classType)?.toEflObject()

    public actual fun newChildrenIterator(): EinaIterator = efl_children_iterator_new(eoPtr).toEinaIterator()

    public actual fun stopEventHandler() {
        efl_event_callback_stop(eoPtr)
    }
}
