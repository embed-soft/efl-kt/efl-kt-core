package io.gitlab.embedSoft.eflKt.core

import io.gitlab.embedSoft.eflKt.core.dataType.EinaIterator

public expect interface EflObjectBase {
    /**
     * Will be *true* if the object is invalidated, *false* otherwise.
     */
    public open val invalidated: Boolean

    /**
     * Will be *true* if the object is invalidating, *false* otherwise.
     */
    public open val invalidating: Boolean

    /**
     * The freeze event count of this object.
     * @since EFL v1.22
     */
    public open val freezeEventCount: Int

    /** Indicates how many references are held on this object. */
    public open val refCount: Int

    /**
     * A human readable comment for the object.
     * @since EFL v1.22
     */
    public open var comment: String

    /**
     * The name of the object.
     * @since EFL v1.22
     */
    public open var name: EflObjectName

    /**
     * The parent of the object.
     * @since EFL v1.22
     */
    public open var parent: EflObjectBase?

    /** Removes a single reference from this object. */
    public open fun unReference()

    /** Freeze all events for this object. */
    public open fun freezeAllEvents()

    /** Thaw all events for this object. */
    public open fun thawAllEvents()

    /**
     * Get an iterator on all children.
     * @return An iterator.
     * @since EFL v1.22
     */
    public open fun newChildrenIterator(): EinaIterator

    /**
     * Stop the current event handler call. This stops the current event handler call. Any other event handlers for the
     * current event will **NOT** be called. This is useful when you want to filter out events. Just add higher
     * priority events, and call this under certain conditions to block a certain event.
     * @since EFL v1.22
     */
    public open fun stopEventHandler()

    public companion object {
        /**
         * Globally freeze events for **ALL** EFL objects. Prevents event handlers from being called for all EFL
         * objects. Enable events again using Efl.Object.event_global_thaw. Events marked hot cannot be stopped.
         * @since EFL v1.22
         */
        public fun freezeAllGlobalEvents()

        /**
         * Thaws events for **ALL** EFL objects.
         * @since EFL v1.22
         */
        public fun thawAllGlobalEvents()
    }
}
