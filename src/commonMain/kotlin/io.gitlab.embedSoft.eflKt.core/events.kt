package io.gitlab.embedSoft.eflKt.core

public typealias EventHandler<T> = (T) -> Unit

@Suppress("SuspiciousCollectionReassignment")
public class Event<T : Any?> {
    private var handlers = emptyList<EventHandler<T>>()

    public fun subscribe(handler: EventHandler<T>) {
        handlers += handler
    }

    public fun unsubscribe(handler: EventHandler<T>) {
        handlers -= handler
    }

    public operator fun plusAssign(handler: EventHandler<T>): Unit = subscribe(handler)

    public operator fun minusAssign(handler: EventHandler<T>): Unit = unsubscribe(handler)

    public operator fun invoke(value: T) {
        var exception: Throwable? = null
        for (handler in handlers) {
            try {
                handler(value)
            } catch (ex: Throwable) {
                exception = ex
            }
        }
        if (exception != null) throw exception
    }
}
