package io.gitlab.embedSoft.eflKt.core

public interface DisposableCallbackData {
    /**
     * Disposes of the stable reference (provides callback data). This function is used in cases where the object is
     * being managed somewhere else, and calling the `close` function would cause a segmentation fault.
     */
    public fun disposeStableReference()
}
