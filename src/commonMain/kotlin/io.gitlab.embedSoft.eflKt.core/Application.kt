package io.gitlab.embedSoft.eflKt.core

/** Represents an application. */
public expect class Application : EflObjectBase {
    /** The EFL build version. */
    public val eflBuildVersion: EflVersion

    /** The EFL version. */
    public val eflVersion: EflVersion

    public companion object {
        /**
         * Gets the default instance of [Application].
         * @return An instance of [Application].
         */
        public fun getMain(): Application
    }
}
