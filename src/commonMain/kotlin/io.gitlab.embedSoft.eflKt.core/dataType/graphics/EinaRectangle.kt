package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import io.gitlab.embedSoft.eflKt.core.Closable

/** A rectangle in pixel dimensions. */
public expect class EinaRectangle : Closable {
    /** X position in pixels from the top-left corner. */
    public var xPos: Int
    /** Y position in pixels from the top-left corner. */
    public var yPos: Int
    /** Width of the rectangle in pixels. */
    public var width: Int
    /** Height of the rectangle in pixels. */
    public var height: Int

    public companion object {
        public fun create(): EinaRectangle
    }
}
