package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import io.gitlab.embedSoft.eflKt.core.Closable

/** Provides a 2D location in pixels. */
public expect class EinaPosition2D : Closable {
    /** X position in pixels from the top-left corner. */
    public var xPos: Int
    /** Y position in pixels from the top-left corner. */
    public var yPos: Int

    public companion object {
        public fun create(): EinaPosition2D
    }
}
