package io.gitlab.embedSoft.eflKt.core.dataType.graphics

import io.gitlab.embedSoft.eflKt.core.Closable

/** Represents a 2D size in pixels. */
public expect class EinaSize2D : Closable {
    /** Width in pixels. */
    public var width: Int
    /** Height in pixels. */
    public var height: Int

    public companion object {
        public fun create(): EinaSize2D
    }
}
