package io.gitlab.embedSoft.eflKt.core.dataType

import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData

/** Represents a hash table (similar to Kotlin's [Map] data type) as a data type. */
public expect class EinaHashTable : Closable, DisposableCallbackData {
    /** Calls the forEach handlers on each element in the hash table. */
    public fun forEach()

    public companion object {
        /**
         * Creates a new hash table with [String] keys.
         * @param algorithm The algorithm to use.
         * @return A new [EinaHashTable] instance.
         */
        public fun createWithStringKeys(algorithm: String = "djb2"): EinaHashTable

        /**
         * Creates a new hash table with [Int] keys.
         * @return A new [EinaHashTable] instance.
         */
        public fun createWithIntKeys(): EinaHashTable

        /**
         * Creates a new hash table with [Long] keys.
         * @return A new [EinaHashTable] instance.
         */
        public fun createWithLongKeys(): EinaHashTable
    }
}
