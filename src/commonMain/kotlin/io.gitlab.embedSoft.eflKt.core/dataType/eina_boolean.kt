package io.gitlab.embedSoft.eflKt.core.dataType

public val Boolean.uByteValue: UByte
    get() = if (this) 1.toUByte() else 0.toUByte()

public val UByte.booleanValue: Boolean
    get() =
        when {
            this == 1.toUByte() -> true
            this == 0.toUByte() -> false
            else -> throw IllegalStateException("UByte must be 1 or 0")
        }
