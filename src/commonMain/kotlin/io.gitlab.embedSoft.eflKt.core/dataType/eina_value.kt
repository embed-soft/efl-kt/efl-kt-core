package io.gitlab.embedSoft.eflKt.core.dataType

import io.gitlab.embedSoft.eflKt.core.Closable

public expect class EinaValue : Closable {
    public fun getString(): String

    public fun setString(newValue: String)

    public fun getChar(): Char

    public fun setChar(newValue: Char)

    public fun getBoolean(): Boolean

    public fun setBoolean(newValue: Boolean)

    public fun getDouble(): Double

    public fun setDouble(newValue: Double)

    public fun getFloat(): Float

    public fun setFloat(newValue: Float)

    public fun getShort(): Short

    public fun setShort(newValue: Short)

    public fun getUShort(): UShort

    public fun setUShort(newValue: UShort)

    public fun getInt(): Int

    public fun setInt(newValue: Int)

    public fun getUInt(): UInt

    public fun setUInt(newValue: UInt)

    public fun getULong(): ULong

    public fun setULong(newValue: ULong)

    public fun getLong(): Long

    public fun setLong(newValue: Long)

    public companion object {
        public fun createStringValue(initialValue: String = ""): EinaValue

        public fun createCharValue(initialValue: Char = '_'): EinaValue

        public fun createBooleanValue(initialValue: Boolean = true): EinaValue

        public fun createDoubleValue(initialValue: Double = 0.0): EinaValue

        public fun createFloatValue(initialValue: Float = 0F): EinaValue

        public fun createShortValue(initialValue: Short = 0.toShort()): EinaValue

        public fun createUShortValue(initialValue: UShort = 0.toUShort()): EinaValue

        public fun createIntValue(initialValue: Int = 0): EinaValue

        public fun createUIntValue(initialValue: UInt = 0u): EinaValue

        public fun createLongValue(initialValue: Long = 0L): EinaValue

        public fun createULongValue(initialValue: ULong = 0uL): EinaValue
    }
}
