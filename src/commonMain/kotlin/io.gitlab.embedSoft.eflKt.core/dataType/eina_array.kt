package io.gitlab.embedSoft.eflKt.core.dataType

import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData

/** Represents an array as a data type. */
public expect class EinaArray : Closable, DisposableCallbackData {
    /** The number of elements in the array. */
    public val size: UInt

    /** Calls the forEach handlers on each element in the array. */
    public fun forEach()

    /**
     * Gets an element from the array as a [String] using [index].
     * @param index The index to use to get the element.
     * @return An element as a [String].
     */
    public infix fun getString(index: UInt): String

    public companion object {
        /**
         * Creates a new instance of [EinaArray].
         * @param size The number of elements to have in the array.
         * @return A new instance of [EinaArray].
         */
        public fun create(size: UInt): EinaArray
    }
}
