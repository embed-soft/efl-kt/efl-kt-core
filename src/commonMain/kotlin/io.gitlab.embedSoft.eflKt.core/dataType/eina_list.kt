package io.gitlab.embedSoft.eflKt.core.dataType

import io.gitlab.embedSoft.eflKt.core.Closable

/** Represents a hash table (similar to Kotlin's [List] data type) as a data type. */
public expect class EinaList : Closable {
    /** The number of elements in the list. */
    public val size: UInt

    /** Reverses all the elements in the list. */
    public fun reverse()

    public companion object {
        /**
         * Creates a new list.
         * @return A new [EinaList] instance.
         */
        public fun create(): EinaList
    }
}
