package io.gitlab.embedSoft.eflKt.core.dataType

import io.gitlab.embedSoft.eflKt.core.Closable
import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData

/** Provides sequential iteration over items in a container (eg [EinaArray]). */
public expect class EinaIterator : Closable, DisposableCallbackData {
    /** Calls the forEach handlers on each item in the container. */
    public fun forEach()
}
