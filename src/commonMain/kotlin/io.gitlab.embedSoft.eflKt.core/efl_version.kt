package io.gitlab.embedSoft.eflKt.core

/**
 * Provides meta data on the EFL version.
 */
public data class EflVersion(
    /** Major component of the version (>= 1). */
    val major: Int,
    /** Minor component of the version (>= 0). */
    val minor: Int,
    /** Micro component of the version (>= 0). */
    val micro: Int,
    /** Revision component of the version (>= 0). */
    val revision: Int,
    /**
     * Special version string for this build of EFL, *""* (an empty [String]) for vanilla (upstream) EFL. Contains
     * `EFL_VERSION_FLAVOR`.
     */
    val flavor: String,
    /** Contains `EFL_BUILD_ID` */
    val buildId: String
)
