package io.gitlab.embedSoft.eflKt.core

/** Represents a basic EFL object. */
public expect class EflObject : EflObjectBase
