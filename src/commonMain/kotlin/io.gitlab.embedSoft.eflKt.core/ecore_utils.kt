package io.gitlab.embedSoft.eflKt.core

/**
 * Sets up connections, event handlers, sockets etc. Note that this function will call [initEina].
 * @return A value of *1* or greater on success, otherwise *0*.
 */
public expect fun initEcore(): Int

/**
 * Shuts down connections, event handlers, sockets etc.
 * @return A value of *0* on success, otherwise a value greater than *0* on failure.
 */
public expect fun shutdownEcore(): Int

/**
 * Shuts down connections, event handlers, sockets etc. Do not call this function from any callback that may be called
 * from the main loop, as the main loop will then fall over and not function properly. This function should be called
 * in symmetry to `initEcoreEx`.
 * @return A value of *0* on success, otherwise a value greater than *0* on failure.
 */
public expect fun shutdownEcoreEx(): UInt

/**
 * This function will propagate the events on the main loop. So you should call [initEcore] first, then register your
 * callback on `EFL_LOOP_EVENT_ARGUMENTS` and finally call [initEcoreEx]. Once you have shutdown down your program, you
 * should symmetrically call [shutdownEcoreEx].
 * @param args The array containing the program arguments to pass through.
 */
public expect fun initEcoreEx(args: Array<String>): UInt
