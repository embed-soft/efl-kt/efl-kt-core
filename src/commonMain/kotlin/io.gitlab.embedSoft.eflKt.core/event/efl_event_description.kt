package io.gitlab.embedSoft.eflKt.core.event

/** The description of a specific event. */
public data class EflEventDescription(
    /** The name of the event. */
    val name: EflEventName,
    /** If the event cannot be frozen. */
    val unfreezable: Boolean,
    /** If when the event is triggered again from a callback it'll start from where it was. */
    val restart: Boolean
)
