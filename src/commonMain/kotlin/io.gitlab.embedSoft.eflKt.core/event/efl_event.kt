package io.gitlab.embedSoft.eflKt.core.event

public value class EflEventName(public val name: String)
