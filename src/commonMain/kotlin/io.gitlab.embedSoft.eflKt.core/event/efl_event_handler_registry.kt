package io.gitlab.embedSoft.eflKt.core.event

import io.gitlab.embedSoft.eflKt.core.DisposableCallbackData

/**
 * Contains all event handlers for a single EFL object.
 */
public expect class EflEventHandlerRegistry : DisposableCallbackData {
    /**
     * Removes an event handler by [event name][eventName].
     * @param eventName The name of the event.
     * @return A value of *true* if an event handler was removed.
     */
    public fun unregister(eventName: EflEventName): Boolean
}
