package io.gitlab.embedSoft.eflKt.core

import io.gitlab.embedSoft.eflKt.core.dataType.EinaValue

/**
 * Starts an event loop.
 * @param obj The object that represents the event loop.
 * @return The return value of this function.
 */
public expect fun beginLoop(obj: EflObject): EinaValue

/**
 * Gets the main loop.
 * @return The object representing the main loop.
 */
public expect fun getMainLoop(): EflObject

/** Starts the main event loop. */
public expect fun beginMainLoop()
