package io.gitlab.embedSoft.eflKt.core

public value class EflObjectName(public val name: String)

/**
 * Exits the application.
 * @param exitCode The application's exit code.
 */
public expect fun exitApplication(exitCode: Int = 0)

/**
 * Adds the event handler for the main event loop.
 * @return A value of *true* if the callback was successfully added.
 */
public expect fun addMainLoopHandler(): Boolean
