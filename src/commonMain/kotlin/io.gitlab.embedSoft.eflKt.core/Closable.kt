package io.gitlab.embedSoft.eflKt.core

/** For types that can be closed. */
public interface Closable {
    public fun close()
}
