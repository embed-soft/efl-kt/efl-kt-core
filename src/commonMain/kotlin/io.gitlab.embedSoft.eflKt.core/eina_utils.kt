package io.gitlab.embedSoft.eflKt.core

/**
 * Initializes the Eina library so that the Eina API can be used.
 * @return A value of *0* if the initialization was successful, or a negative number on failure.
 */
public expect fun initEina(): Int
